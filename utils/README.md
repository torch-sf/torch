README

This is a place for user-contributed utility tools, scripts, etc. to aid in the
setup, running, and/or analysis of Torch simulations.

All contributions are welcome!
