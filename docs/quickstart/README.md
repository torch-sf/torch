README

__Torch quickstart guide.__  Most recent version is "-web".  This is the
quickstart guide posted at:
[https://torch-sf.bitbucket.io/quickstart.pdf](https://torch-sf.bitbucket.io/quickstart.pdf).
Relevant files are:

	quickstart-cca.tex
	quickstart-web.tex
	quickstart-dens-slice.pdf
	quickstart-particles.pdf
	fig/torch-features.pdf

quickstart-cca.tex = version from August 28-30, 2019 workshop hosted by
Flatiron Institute CCA
