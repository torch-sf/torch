Updated: 04/03/2020 -- Sean C. Lewis


This is meant to be a beginners guide to creating a new git branch to work on a feature in and how to push/merge those changes into the main project.

To view the guide, simply generate the pdf from the .tex file here by issuing the following command in this directory.


pdflatex AddingNewFeatures.tex