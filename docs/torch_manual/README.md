README

__Various notes on Torch ("A Torch Manual").__  Some overlap with
quickstart (basically, a subset of these notes evolved into the quickstart).
A copy is posted at:
[http://user.astro.columbia.edu/~atran/torch/manual.pdf](http://user.astro.columbia.edu/~atran/torch/manual.pdf).
Relevant files are:

	main.tex

Scripts to make figures:

	cool.dat
	dust_heat_cool.py
	dust_opacity.py
	gas_cool.py
	pe_heating.py
	pe_heating_expressions.py
	recombination_case_b.py
